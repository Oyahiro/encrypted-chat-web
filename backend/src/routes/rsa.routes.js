const express = require('express');
const router = express.Router();
let QuickEncrypt = require('quick-encrypt')

router.get('/crear-rsa', async(req, res) => {
    let keys = QuickEncrypt.generate(1024);
    res.json({keys});
});

router.post('/encriptar', async(req, res) => {
    let publicKey = req.body.publicKey;
    let hash = req.body.hash;
    let keyCrypt = QuickEncrypt.encrypt(hash, publicKey);
    res.json({keyCrypt});
});

router.post('/desencriptar', async(req, res) => {
    let privateKey = req.body.privateKey;
    let hash = req.body.hash;
    let keyCrypt = QuickEncrypt.decrypt(hash, privateKey)
    res.json({keyCrypt});
});

module.exports = router;