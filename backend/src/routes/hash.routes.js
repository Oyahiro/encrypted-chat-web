const express = require('express');
const router = express.Router();
var CryptoJS = require("crypto-js");

router.post('/encriptar', async(req, res) => {
    let msg = req.body.msg;
    let key = req.body.key;
    var textCypt = CryptoJS.AES.encrypt(msg, key).toString();
    res.json({"msg": textCypt});
});

router.post('/desencriptar', async(req, res) => {
    let msg = req.body.msg;
    let key = req.body.key;
    var bytes  = CryptoJS.AES.decrypt(msg, key);
    var text = bytes.toString(CryptoJS.enc.Utf8);
    res.json({"msg": text});
});

module.exports = router;