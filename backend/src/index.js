const express = require('express');
const morgan = require('morgan');
const http = require('http');
const cors = require('cors');
const app = express();
const server = http.createServer(app);

// settings
app.set('port', process.env.PORT || 3000);

// middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}))
app.use(express.json());
app.use(cors());

// routes
app.use('/rsa',require('./routes/rsa.routes'));
app.use('/hash', require('./routes/hash.routes'));

// starting the server
server.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`)
});
