// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyBVqRckPJey89IMvuf4jHfUoNXLpB5Kk2c",
  authDomain: "chat-seguridades.firebaseapp.com",
  databaseURL: "https://chat-seguridades.firebaseio.com",
  projectId: "chat-seguridades",
  storageBucket: "chat-seguridades.appspot.com",
  messagingSenderId: "162497135920",
  appId: "1:162497135920:web:528dea15b47431830ab4f5"
};

export const firebaseStorage = [
  'https://firebasestorage.googleapis.com/v0/b/chat-seguridades.appspot.com/o/chat_1.png?alt=media&token=4ef9badb-18d5-4cf9-a440-2dc99c629df9',
  'https://firebasestorage.googleapis.com/v0/b/chat-seguridades.appspot.com/o/chat_2.png?alt=media&token=ef06f614-f695-4303-a7c0-16b0f5fbb999',
  'https://firebasestorage.googleapis.com/v0/b/chat-seguridades.appspot.com/o/chat_3.png?alt=media&token=89ef7b09-cb89-4928-b7f3-60ecc1bb372b',
  'https://firebasestorage.googleapis.com/v0/b/chat-seguridades.appspot.com/o/chat_4.png?alt=media&token=6c1f0b62-a3f5-4c4b-a058-a8872be97bd1',
  'https://firebasestorage.googleapis.com/v0/b/chat-seguridades.appspot.com/o/chat_5.png?alt=media&token=3a21ed25-9f12-4c8c-afc4-e266bfac03d9',
  'https://firebasestorage.googleapis.com/v0/b/chat-seguridades.appspot.com/o/chat_6.png?alt=media&token=0fa228c8-7e70-4632-ac79-329172958e68',
  'https://firebasestorage.googleapis.com/v0/b/chat-seguridades.appspot.com/o/chat_7.png?alt=media&token=a018ac9c-6525-4217-bd63-c055f314d1e5',
  'https://firebasestorage.googleapis.com/v0/b/chat-seguridades.appspot.com/o/chat_8.png?alt=media&token=83c3e33e-f299-424d-8734-d06871201800',
  'https://firebasestorage.googleapis.com/v0/b/chat-seguridades.appspot.com/o/chat_9.png?alt=media&token=2843e711-37cf-4571-ac58-9ec2ab76bdd4',
  'https://firebasestorage.googleapis.com/v0/b/chat-seguridades.appspot.com/o/chat_10.png?alt=media&token=b6db059d-659e-4297-9ee2-663da6fd1792'
]

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
