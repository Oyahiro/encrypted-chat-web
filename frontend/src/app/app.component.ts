import { Component } from '@angular/core';

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

import { AuthService } from './services/auth.service';

import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Chat';

  constructor(public dialog: MatDialog,
    public authService:  AuthService) {

  }

  showLoginDialog() {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '400px',
      data: {email: "", password: ""}
    });
  }

  showRegisterDialog() {
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '400px',
      data: {email: "", password: ""}
    });
  }

  onLogOut() {
    this.authService.logout();
  }
}
