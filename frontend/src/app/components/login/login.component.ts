import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from  '../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(public dialog: MatDialog,
    public  authService:  AuthService, 
    private formBuilder: FormBuilder, 
    public dialogRef: MatDialogRef<LoginComponent>,
    @Inject(MAT_DIALOG_DATA) public data: LoginComponent,
    private _snackBar: MatSnackBar
    ) { }

  ngOnInit(): void {
    this.loginForm  =  this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required]
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  
  login() {
    let email = this.loginForm.get('email').value;
    let password = this.loginForm.get('password').value;
    this.authService.login(email, password)
      .then(res => {
        this.cancel();
      })
      .catch(err => {
        this.openSnackBar('Error al iniciar sesion', 'Cerrar');
        this.cancel();
      });
  }

  cancel(){
    this.dialogRef.close();
  }
}
