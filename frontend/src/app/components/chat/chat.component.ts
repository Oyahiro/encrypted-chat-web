import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { DbService } from '../../services/db.service';
import { AuthService } from '../../services/auth.service';
import { CryptoService } from '../../services/crypto.service';
import { Message } from './../../models/Message';
import { User } from '../../models/User';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  @Input()
  chat_id:string;

  hash_key:string = '';
  messages = [ ];
  contentMessage:string;
  conversation:any;
  user:User;
  activeChat:boolean = false;

  constructor(private dbService:DbService,
    public authService:AuthService,
    public cryptoServie:CryptoService) { }

  ngOnInit(): void {
    this.user = JSON.parse(window.localStorage.getItem('user')) as User;
  }

  ngOnChanges(changes: SimpleChanges) {
    if(this.chat_id !== ''){
      this.contentMessage = '';
      this.dbService.getConversation(this.chat_id)
        .subscribe(conversation => {
          this.conversation = conversation;
          this.activeChat = true;
          if(this.conversation.user1.id===this.user.id) {
            this.cryptoServie.decryptHashKey(this.user.privateKey, this.conversation.user1.key)
              .subscribe(res => {
                let keyObj:any = res;
                this.hash_key = keyObj.keyCrypt;
                this.decryptMessages();
              });
          } else {
            this.cryptoServie.decryptHashKey(this.user.privateKey, this.conversation.user2.key)
              .subscribe(res => {
                let keyObj:any = res;
                this.hash_key = keyObj.keyCrypt;
                this.decryptMessages();
              });
          }
        });
    }
  }
  
  sendMessage() {
    this.cryptoServie.encryptMessage(this.hash_key, this.contentMessage)
      .subscribe(res => {
        let resObj:any = res;
        const message:Message = {
          content : resObj.msg,
          user_id: this.user.id,
          user_name: this.user.name
        }
        this.dbService.sendMessage(message, this.chat_id);
        this.contentMessage = '';
      });
  }

  decryptMessages() {
    let aux = 0;
    if(this.conversation.messages){
      for(let msg of this.conversation.messages) {
        this.cryptoServie.decryptMessage(this.hash_key, msg.content)
          .subscribe(res => {
            let resObj:any = res;
            this.conversation.messages[aux].content = resObj.msg;
            aux+=1;
          });
      }
    }
  }
}
