import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from  '../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CryptoService } from '../../services/crypto.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  keys: any = {
    public: '',
    private: ''
  }

  constructor(public dialog: MatDialog,
    public  authService:  AuthService, 
    private formBuilder: FormBuilder, 
    public dialogRef: MatDialogRef<RegisterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RegisterComponent,
    private _snackBar: MatSnackBar,
    private cryptoService:CryptoService
    ) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  register() {
    let name = this.registerForm.get('name').value;
    let email = this.registerForm.get('email').value;
    let password = this.registerForm.get('password').value;
    let confirmPassword = this.registerForm.get('confirmPassword').value;

    if(password !== confirmPassword) {
      this.openSnackBar('Las contraseñas no coinciden', 'Cerrar');
    }else {
      this.cryptoService.getKeys()
        .subscribe(res => {
          this.keys = res;
          this.authService.register(email, password, name, this.keys.keys.public, this.keys.keys.private)
            .then(auth => {
              this.openSnackBar('Usuario registrado correctamente', 'Cerrar');
              this.cancel();
            }).catch(err => {
              console.log(err)
            });
        })
    }
  }

  cancel(){
    this.dialogRef.close();
  }
}
