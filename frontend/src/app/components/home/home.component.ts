import { Component, OnInit } from '@angular/core';
import { Conversation } from '../../models/Conversation';
import { AuthService } from '../../services/auth.service';
import { DbService } from '../../services/db.service';
import { CryptoService } from '../../services/crypto.service';
import { UserConversation } from './../../models/Conversation';
import { User } from '../../models/User';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  conversation_id:string = '';
  users:any = [];
  user:User;
  userConversation1:UserConversation = {
    id: '',
    name: '',
    key: ''
  };
  userConversation2:UserConversation = {
    id: '',
    name: '',
    key: ''
  };

  constructor(public authService:AuthService,
    public dbService:DbService,
    public cryptoServie:CryptoService) { }

  ngOnInit(): void {
    this.user = JSON.parse(window.localStorage.getItem('user')) as User;

    this.dbService.getUsers()
      .subscribe(users => {
        this.users = users;
        this.deleteMyUser();
      })
  }

  openChat(user) {
    let conversations: any = [];
    let newConversation:boolean = true;
    let conversationAct: any;
    this.setUserConversation(user);

    this.dbService.getConversations()
      .subscribe(res => {
        conversations = res as Conversation;
        for(let c of conversations) {
          if((c.user1.id===this.user.id && c.user2.id===user.id) || (c.user1.id===user.id && c.user2.id===this.user.id)) {
            newConversation = false;
            conversationAct = c;
            break;
          }
        }
        if(newConversation)
          this.createConversation();
        else 
          this.conversation_id = conversationAct.id;
      })
  }

  createConversation(){
    this.dbService.createConversation(this.userConversation1, this.userConversation2)
      .then(res => {
        this.conversation_id = res.id;
      }).catch(err => {
        console.log(err);
      });
  }

  setUserConversation(user) {
    let hash = Math.round(Math.random() * (100 - 999)) + 100;
    this.cryptoServie.encryptHashKey(this.user.publicKey, hash.toString())
      .subscribe(res => {
        let keyCrypObj:any = res;
        this.userConversation1.id = this.user.id;
        this.userConversation1.name = this.user.name;
        this.userConversation1.key = keyCrypObj.keyCrypt;
      })
    
    this.cryptoServie.encryptHashKey(user.publicKey, hash.toString())
      .subscribe(res => {
        let keyCrypObj:any = res;
        this.userConversation2.id = user.id;
        this.userConversation2.name = user.name;
        this.userConversation2.key = keyCrypObj.keyCrypt;
      })
  }

  deleteMyUser(){
    if(this.authService.isLoggedIn){
      for(let user of this.users) {
        var userAct = user as User;
        if(userAct.id===this.user.id){
          var i = this.users.indexOf(user);
          this.users.splice(i, 1);
        }
      }
    }
  }
}
