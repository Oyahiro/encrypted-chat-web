import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  readonly URL = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }
  
  getKeys() {
    return this.http.get(this.URL + '/rsa/crear-rsa');
  }

  encryptHashKey(publicKey:string, hash:string) {
    let body:any = {
      publicKey: publicKey,
      hash: hash
    }
    return this.http.post(this.URL + '/rsa/encriptar', body);
  }

  decryptHashKey(privateKey:string, hash:string) {
    let body:any = {
      privateKey: privateKey,
      hash: hash
    }
    return this.http.post(this.URL + '/rsa/desencriptar', body);
  }

  encryptMessage(key:string, msg:string) {
    let body:any = {
      key: key,
      msg: msg
    }
    return this.http.post(this.URL + '/hash/encriptar', body);
  }

  decryptMessage(key:string, msg:string) {
    let body:any = {
      key: key,
      msg: msg
    }
    return this.http.post(this.URL + '/hash/desencriptar', body);
  }
}
