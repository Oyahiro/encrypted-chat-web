import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Message } from '../models/Message';
import { Conversation } from '../models/Conversation';
import { User } from '../models/User';
import { map } from 'rxjs/operators';
import { firestore } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class DbService {

  constructor(private db:AngularFirestore) { }

  getUsers(){
    return this.db.collection('user')
      .snapshotChanges()
      .pipe(map(rooms => {
        return rooms.map(a => {
          const data = a.payload.doc.data() as User;
          data.id = a.payload.doc.id;
          return data;
        })
      }));
  }

  getConversations(){
    return this.db.collection('conversation')
      .snapshotChanges()
      .pipe(map(rooms => {
        return rooms.map(a => {
          const data = a.payload.doc.data() as Conversation;
          data.id = a.payload.doc.id;
          return data;
        })
      }));
  }

  getConversation(chat_id:string) {
    return this.db.collection('conversation')
      .doc(chat_id)
      .valueChanges();
  }

  createConversation(user1:User, user2:User) {
    return this.db.collection('conversation')
      .add({
        user1: user1,
        user2: user2
      })
  }

  sendMessage(message:Message, chat_id:string) {
    this.db.collection('conversation')
      .doc(chat_id)
      .update({
        messages: firestore.FieldValue.arrayUnion(message)
      })
  }
}
