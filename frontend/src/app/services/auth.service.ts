import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { User } from '../models/User';
import { firebaseStorage } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private auth: AngularFireAuth,
    public router:Router,
    private db:AngularFirestore) { }

  register(email:string, password:string, name:string, publicK:string, privateK:string) {
    return new Promise((resolve, reject) => {
      this.auth.createUserWithEmailAndPassword(email, password)
        .then(res => {
          const uid = res.user.uid;
          this.db.collection('user')
            .doc(uid)
            .set({
              name: name,
              img: this.getRandomUrl(),
              publicKey: publicK,
              privateKey: privateK
            })
          this.setLocalUser(uid);
          resolve(res)
        }).catch(err => {
          reject(err)
        });
    })
  }

  login(email:string, password:string) {
    return new Promise((resolve, rejected) => {
      this.auth.signInWithEmailAndPassword(email, password)
        .then(user => {
          this.setLocalUser(user.user.uid);
          resolve(user);
        })
        .catch(err => {
          rejected(err)
        })
    });
  }

  logout() {
    this.auth.signOut()
      .then(auth => {
        localStorage.removeItem('user');
      });
  }

  setLocalUser(uid:string) {
    this.db.collection('user').doc(uid).valueChanges().subscribe(data => {
      var user = data as User;
      user.id = uid;
      localStorage.setItem('user', JSON.stringify(user));
    })
  }

  get isLoggedIn(): boolean {
		const user = JSON.parse(localStorage.getItem('user'));
	    return user!==null;
  }
  
  getRandomUrl() {
    return firebaseStorage[Math.round(Math.random()*10)];
  }
}
