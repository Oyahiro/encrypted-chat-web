export interface UserConversation {
  id?:string;
  name?:string;
  key?:string;
}

export interface Conversation {
  id?:string;
  user1?: UserConversation;
  user2?: UserConversation;
}