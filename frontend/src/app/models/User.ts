export interface User {
    id?: string;
    name?: string;
    img?: string;
    publicKey?: string;
    privateKey?: string;
}